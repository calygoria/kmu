# KMU

## How to deploy on `calygoria.fr`

```bash
cd /home/calan/kmu
git fetch && git pull
./gradlew build install -x test
./docker_build.sh
docker compose up -d
```