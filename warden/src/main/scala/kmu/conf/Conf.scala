package kmu.conf

import com.typesafe.config.ConfigFactory
import pureconfig.ConfigSource
import pureconfig.generic.auto._

case class Conf(
  global: GlobalConf
){
}

case class GlobalConf(
  warden: WardenConf,
  api: ApiConf
)

case class WardenConf(
  isEnabled: Boolean
)

case class ApiConf(
  password: String
)

object Conf {

  def load(): Conf = ConfigSource.fromConfig(ConfigFactory.load()).loadOrThrow[Conf]
}



trait KmuConf {

  implicit val conf: Conf = Conf.load()

}