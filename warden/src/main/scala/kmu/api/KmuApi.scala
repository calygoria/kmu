package kmu.api

import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.Credentials
import ch.megard.akka.http.cors.scaladsl.CorsDirectives._
import de.heikoseeberger.akkahttpjackson.JacksonSupport
import kmu.app.ApplicationComponent
import kmu.conf.KmuConf
import kmu.log.Logging
import kmu.reminders.{Birthday, BirthdayRepository, BirthdayRepositoryComponent}
import kmu.sqlite.SqliteServiceComponent

import scala.language.postfixOps

class KmuApi(implicit val birthdayRepository: BirthdayRepository) extends JacksonSupport with Logging with KmuConf {

  private def authenticator(credentials: Credentials): Option[String] =
    credentials match {
      case p@Credentials.Provided(id) if p.verify(conf.global.api.password) => Some(id)
      case _ => None
    }


  val topLevelRoute: Route = cors () {
    authenticateBasic(realm = "kmu", authenticator) { _ =>
      concat(
        pathPrefix("birthday")(birthdayRoute)
        // add the reminder route
      )
    }
  }

  private lazy val birthdayRoute: Route = {
    concat(
      pathEnd {
        concat(
          get {
            complete {
              // Return all birthdays
              birthdayRepository.findAll()
            }
          },
          post {
            entity(as[Birthday]) { birthday =>
              complete {
                // Save birthday to the db and return it
                birthdayRepository.insertBirthday(birthday)
              }
            }
          }
        )
      },
      path(IntNumber) { id =>
        concat(
          get {
            complete {
              // Return the matching birthday
              birthdayRepository.findById(id) match {
                case Some(value) => value
                case None => StatusCodes.NotFound
              }
            }
          },
          delete {
            complete {
              // Delete and returns 200
              birthdayRepository.deleteById(id)
              StatusCodes.OK
            }
          },
          put {
            entity(as[Birthday]) { birthday =>
              complete {
                // Returns the overwritten birthday
                birthdayRepository.update(birthday)
              }
            }
          }
        )
      }
    )
  }

  lazy val route: Route =
    get {
      complete{
        HttpEntity(ContentTypes.`text/html(UTF-8)`, "<h1>Say hello to akka-http</h1>")
        "<h1>Hello monde</h1>"
      }
    }

}

trait KmuApiComponent {
  this: ApplicationComponent
    with SqliteServiceComponent
    with BirthdayRepositoryComponent
    with KmuConf
    with Logging =>

  implicit val kmuAPi: KmuApi = new KmuApi()

  log.info("Starting app : KMU API")
  Http().newServerAt("0.0.0.0", 8080).bind(kmuAPi.topLevelRoute)
}