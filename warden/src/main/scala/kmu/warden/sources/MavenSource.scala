package kmu.warden.sources

import akka.actor.ActorSystem
import kmu.app.ApplicationComponent
import kmu.http.HttpClient
import kmu.warden.artifacts.Artifact

import scala.concurrent.{ExecutionContext, Future}
import scala.xml.XML

// To scrap the latest version of a maven source, go to https://mvnrepository.com/{my_artifact}/latest
// It will automatically redirect to the latest version
// Well, yes but no, because anti-bot protection
// So we will have to parse maven-metadata.xml at https://repo1.maven.org/maven2/
class MavenSource(implicit val httpClient: HttpClient, val system: ActorSystem) extends Source {

  private val blockingIOExecutor: ExecutionContext = system.dispatchers.lookup("akka.actor.default-blocking-io-dispatcher")

  private val mavenRepository: String = "https://repo1.maven.org/maven2"
  private val metadataFile: String = "maven-metadata.xml"

  override def name(): String = "maven"

  override def scrap(artifact: Artifact): Future[Artifact] = {

    httpClient
      .get(s"$mavenRepository/${artifact.path}/$metadataFile")
      .map{ response =>
        val metadata = XML.loadString(response.utf8String)
        val version = (metadata \ "versioning" \ "latest").text
        Artifact(artifact.source, artifact.name, artifact.path, artifact.releasePath, version)
      }(blockingIOExecutor)
  }
}

trait MavenSourceComponent {
  this: ApplicationComponent =>

  lazy val mavenSource = new MavenSource()
}
