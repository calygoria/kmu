package kmu.warden.sources

import akka.actor.ActorSystem
import kmu.app.ApplicationComponent
import kmu.http.HttpClient
import kmu.warden.artifacts.Artifact

import scala.concurrent.{ExecutionContext, Future}

class GithubSource(implicit val httpClient: HttpClient, val system: ActorSystem) extends Source {

  private val blockingIOExecutor: ExecutionContext = system.dispatchers.lookup("akka.actor.default-blocking-io-dispatcher")

  private val github: String = "https://github.com/"
  private val latestRelease: String = "releases/latest"

  override def name(): String = "github"

  override def scrap(artifact: Artifact): Future[Artifact] = {

    httpClient
      .getHttpResponse(s"$github/${artifact.path}/$latestRelease")
      .map { response =>
        val version = response
          .headers
          .find(_.name == "Location").get
          .value()
          .split("/")
          .last

        Artifact(artifact.source, artifact.name, artifact.path, artifact.releasePath, version)
      }(blockingIOExecutor)

  }
}

trait GithubSourceComponent {
  this: ApplicationComponent =>

  lazy val githubSource = new GithubSource()
}