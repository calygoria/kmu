package kmu.warden.sources

import kmu.json.ObjectMapperComponent
import kmu.log.Logging
import kmu.warden.artifacts.Artifact

import scala.concurrent.Future

trait Source extends Logging with ObjectMapperComponent {

  def name(): String
  def scrap(artifact: Artifact): Future[Artifact]
}
