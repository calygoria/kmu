package kmu.warden

import akka.actor.{Actor, Props, Status}
import com.typesafe.config.Config
import kmu.SUCCESS
import kmu.app.ApplicationComponent
import kmu.conf.KmuConf
import kmu.log.Logging
import kmu.telegram.{TelegramService, TelegramServiceComponent}
import kmu.warden.artifacts.{Artifact, ArtifactRepository, ArtifactRepositoryComponent}
import kmu.warden.sources.{GithubSourceComponent, MavenSourceComponent, Source}

//import scala.collection.JavaConverters.collectionAsScalaIterableConverter
import scala.jdk.CollectionConverters._
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

class Warden(
              val artifactRepository: ArtifactRepository,
              val config: Config,
              val sources: Seq[Source],
              val telegram: TelegramService)
  extends Actor with Logging {

  import context.{dispatcher, system}

  private case object Scrap
  private val scrapInterval = config.getInt("warden.scrapInterval")

  override def preStart(): Unit = {
    initDb()
    system.scheduler.scheduleOnce(3 seconds, self, Scrap)
  }

  override def receive: Receive = {
    case Scrap =>
      log.info("Scraping sources for new updates")
      scrapSources()
      system.scheduler.scheduleOnce(scrapInterval seconds, self, Scrap)

    case Status.Failure(t) =>
      log.error(t.getMessage, t)
  }

  private def initDb(): Unit = {
    if (config.getBoolean("sqlite.dropDatabaseOnStart")) {
      log.info("Dropping artifacts table")
      artifactRepository.dropArtifactsTable()
    }
    log.info("Initializing artifact table")
    artifactRepository.createArtifactsTable()
    config.getObjectList("artifacts")
      .asScala.toSeq
      .map { artifactConfig =>
        Artifact(
          artifactConfig.get("source").unwrapped().toString,
          artifactConfig.get("name").unwrapped().toString,
          artifactConfig.get("path").unwrapped().toString,
          artifactConfig.get("releasePath").unwrapped().toString,
          "NULL"
        )
      }
      .foreach { artifact =>
        artifactRepository.insertArtifact(artifact)
      }
//    artifactRepository.showArtifactTable()

  }

  private def scrapSources(): Unit = {
    log.info("Starting scraping")
    artifactRepository
      .listArtifacts()
      .foreach{ artifact =>
        sources
          .find(_.name() == artifact.source)
          .foreach{ source =>
            log.info(s"Looking for updates on artifact ${artifact.name}")
            val scrapedArtifact = Await.result(source.scrap(artifact), 10 seconds)
            if (artifact != scrapedArtifact) {
              log.info(s"${artifact.name} version changed from ${artifact.version} to ${scrapedArtifact.version}")
              log.info("Sending Telegram message")
              val notificationSent = sendUpdateMessage(artifact, scrapedArtifact)
              if (notificationSent == SUCCESS) {
                log.info("Updating artifact in database")
                artifactRepository.updateArtifact(scrapedArtifact)
              } else {
                log.warn("Could not send Telegram message, retrying later")
              }
            }
          }
      }
    log.info("Scraping done !")
  }

  private def sendUpdateMessage(oldArtifact: Artifact, artifact: Artifact): String = {
    val message =
        s"""
           |✨ *${artifact.name}*
           |
           |`${oldArtifact.version}` ➡ `${artifact.version}`
           |
           |📚 Check release notes [here](${artifact.releasePath})
            """.stripMargin
    Await.result(telegram.send(message), 10 seconds)
  }

}

trait WardenComponent {
  this: ApplicationComponent
    with KmuConf
    with ArtifactRepositoryComponent
    with MavenSourceComponent
    with GithubSourceComponent
    with TelegramServiceComponent
    with Logging =>

  val sources: Seq[Source] = Seq(githubSource, mavenSource)

  if (conf.global.warden.isEnabled) {
    log.info("Starting app : Warden")
    system.actorOf(Props(new Warden(artifactRepository, config, sources, telegram)), "warden")
  } else {
    log.info("Warden is disabled by config, skipping")
  }
}
