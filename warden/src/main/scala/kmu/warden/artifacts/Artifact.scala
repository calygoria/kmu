package kmu.warden.artifacts

case class Artifact(source: String,
                    name: String,
                    path: String,
                    releasePath: String,
                    version: String
                   ) {

  def toValues: String = {
    s""""$source", "$name", "$path", "$releasePath", "$version""""
  }
}

object Artifact {

  val toSchema: String = s"id integer PRIMARY KEY AUTOINCREMENT, source string, name string, path string UNIQUE, release_path string, version string"

  val toInsertSchema: String = "source, name, path, release_path, version"

  val updateField: String = "version"

  val conditionField: String = "path"

}

