package kmu.warden.artifacts

import kmu.SUCCESS
import kmu.sqlite.{SqliteService, SqliteServiceComponent}

class ArtifactRepository(val sqlite: SqliteService) {

  private val artifactTable = "artifacts"

  def createArtifactsTable(): String = {
    sqlite.createTableIfNotExists(artifactTable, Artifact.toSchema)
    SUCCESS
  }

  def dropArtifactsTable(): String = {
    sqlite.dropTable(artifactTable)
    SUCCESS
  }

  def insertArtifact(artifact: Artifact): String = {
    sqlite.insertIfNotExists(artifactTable, Artifact.toInsertSchema, artifact.toValues)
    SUCCESS
  }

  def updateArtifact(artifact: Artifact): String = {
    sqlite.update(artifactTable,
      Artifact.updateField,
      s""""${artifact.version}"""",
      Artifact.conditionField,
      s""""${artifact.path}""""
    )
    SUCCESS
  }

  def showArtifactTable(): String = {
    sqlite.showTable(artifactTable)
    SUCCESS
  }

  def listArtifacts(): Seq[Artifact] = {
    val rs = sqlite.selectAll(artifactTable)
    Iterator.from(0).takeWhile(_ => rs.next())
      .map{ _ =>
        Artifact(
          rs.getString(2),
          rs.getString(3),
          rs.getString(4),
          rs.getString(5),
          rs.getString(6)
        )
      }.toSeq
  }

}

trait ArtifactRepositoryComponent {
  this: SqliteServiceComponent =>

  lazy val artifactRepository = new ArtifactRepository(sqlite)
}
