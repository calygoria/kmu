package kmu

import akka.actor.ActorSystem
import kmu.api.KmuApiComponent
import kmu.app.{Application, ApplicationComponent}
import kmu.conf.KmuConf
import kmu.log.Logging
import kmu.reminders.{BirthdayReminderComponent, BirthdayRepositoryComponent, TrashReminderComponent, EparcylReminderComponent}
import kmu.sqlite.SqliteServiceComponent
import kmu.telegram.TelegramServiceComponent
import kmu.warden.WardenComponent
import kmu.warden.artifacts.ArtifactRepositoryComponent
import kmu.warden.sources.{GithubSourceComponent, MavenSourceComponent}

trait WardenApplication
  extends ApplicationComponent
    with KmuConf
    with SqliteServiceComponent
    with ArtifactRepositoryComponent
    with GithubSourceComponent
    with MavenSourceComponent
    with TelegramServiceComponent
    with Logging
    with WardenComponent

trait KmuApiApplication
  extends ApplicationComponent
    with SqliteServiceComponent
    with BirthdayRepositoryComponent
    with KmuConf
    with Logging
    with KmuApiComponent

trait BirthdayReminderApplication
  extends ApplicationComponent
    with KmuConf
    with SqliteServiceComponent
    with BirthdayRepositoryComponent
    with TelegramServiceComponent
    with Logging
    with BirthdayReminderComponent

trait TrashReminderApplication
  extends ApplicationComponent
    with KmuConf
    with TelegramServiceComponent
    with Logging
    with TrashReminderComponent

trait EparcylReminderApplication
  extends ApplicationComponent
    with KmuConf
    with TelegramServiceComponent
    with Logging
    with EparcylReminderComponent

class Run(implicit val system: ActorSystem)
  extends WardenApplication
    with KmuApiApplication
    with BirthdayReminderApplication
    with TrashReminderApplication
    with EparcylReminderApplication
    with Runnable {

  override def run(): Unit = {
    log.info("All KMU apps have been started")
  }
}

object Run extends Application {

  protected def init(args: Array[String])(implicit system: ActorSystem): Runnable = new Run()
}