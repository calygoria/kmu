package kmu.reminders

case class Birthday(
  id: Option[Int],
  firstName: String,
  lastName: String,
  day: Int,
  month: Int,
  year: Option[Int]
) {
  def toValues: String = {
    if (year.isEmpty){
      s""""$firstName", "$lastName", "$day", "$month", NULL"""
    } else {
      s""""$firstName", "$lastName", "$day", "$month", "${year.get}""""
    }
  }

  def toFieldValueMapping: String = {
    if (year.isEmpty) {
      s"""first_name = "$firstName", last_name = "$lastName", day = "$day", month = "$month", year = NULL"""
    } else {
      s"""first_name = "$firstName", last_name = "$lastName", day = "$day", month = "$month", year = "${year.get}""""
    }
  }

}

object Birthday {
  val toSchema: String = s"id integer PRIMARY KEY AUTOINCREMENT, first_name string, last_name string, day int, month int, year int"
  val toInsertSchema: String = "first_name, last_name, day, month, year"
}