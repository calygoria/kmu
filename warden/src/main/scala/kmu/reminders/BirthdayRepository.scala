package kmu.reminders

import kmu.SUCCESS
import kmu.sqlite.{SqliteService, SqliteServiceComponent}

class BirthdayRepository(val sqlite: SqliteService) {

  private val birthdayTable: String = "birthdays"

  def createTable: String = {
    sqlite.createTableIfNotExists(birthdayTable, Birthday.toSchema)
    SUCCESS
  }

  def dropTable(): String = {
    sqlite.dropTable(birthdayTable)
    SUCCESS
  }

  def findAll(): List[Birthday] = {
    val rs = sqlite.selectAll(birthdayTable)
    Iterator.from(0).takeWhile(_ => rs.next())
      .map { _ =>
        Birthday(
          id = Some(rs.getInt(1)),
          firstName = rs.getString(2),
          lastName = rs.getString(3),
          day = rs.getInt(4),
          month = rs.getInt(5),
          year =
            if (rs.getInt(6) == 0) {
              None
            } else {
              Some(rs.getInt(6))
            }
        )
      }
      .toList
  }

  def findById(id: Int): Option[Birthday] = {
    findAll() // Very dirty, don't do this :)
      .find(b => b.id.contains(id))
  }

  def deleteById(id: Int): Unit = {
    sqlite.delete(birthdayTable, "id", id.toString)
  }

  def update(birthday: Birthday): Option[Birthday] = {
    if (birthday.id.isDefined){
      sqlite.fullUpdate(birthdayTable, birthday.toFieldValueMapping, "id", birthday.id.get.toString)
      findById(birthday.id.get)
    } else {
      None
    }
  }

  def insertBirthday(birthday: Birthday): Option[Birthday] = {
    sqlite.insertIfNotExists(birthdayTable, Birthday.toInsertSchema, birthday.toValues)
    findAll() // Very dirty, don't do this :)
      .find { b => b == birthday}
  }

}

trait BirthdayRepositoryComponent {
  this: SqliteServiceComponent =>

  implicit lazy val birthdayRepository: BirthdayRepository = new BirthdayRepository(sqlite)
}