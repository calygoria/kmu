package kmu.reminders

import akka.actor.{Actor, Props, Status}
import kmu.app.ApplicationComponent
import kmu.conf.KmuConf
import kmu.log.Logging
import kmu.telegram.{TelegramService, TelegramServiceComponent}

import java.time.{DayOfWeek, LocalDate, LocalDateTime, ZoneOffset}
import scala.concurrent.Await
import scala.concurrent.duration.{DurationInt, DurationLong}
import scala.language.postfixOps

class EparcylReminder(
  val telegram: TelegramService
)
  extends Actor with Logging {

  import context.{dispatcher, system}

  private case object Check

  override def preStart(): Unit = {
    system.scheduler.scheduleOnce(3 seconds, self, Check)
  }

  override def receive: Receive = {
    case Check =>
      log.info("Checking eparcyl schedule")
      checkSchedule()
      val now = LocalDateTime.now()
      val nextSchedule = now.plusDays(1).withHour(14).withMinute(0).withSecond(0)
      val scrapInterval = nextSchedule.toEpochSecond(ZoneOffset.UTC) - now.toEpochSecond(ZoneOffset.UTC)
      log.info(s"Next Eparcyl check is schedueled in ${scrapInterval} seconds")
      system.scheduler.scheduleOnce(scrapInterval seconds, self, Check)

    case Status.Failure(t) =>
      log.error(t.getMessage, t)
  }

  private def checkSchedule(): Unit = {
    val today = LocalDate.now()
    if(today.getDayOfWeek == DayOfWeek.TUESDAY) {
      sendReminder()
    }
  }

  private def sendReminder() = {
    val message =
      s"""
         |🚽️ Allez on met un coup d'Eparcyl, la fosse a faim !
      """.stripMargin
    Await.result(telegram.send(message), 10 seconds)
  }
}

trait EparcylReminderComponent {
  this: ApplicationComponent
    with KmuConf
    with TelegramServiceComponent
    with Logging =>

  log.info("Starting app : Eparcyl reminder")
  system.actorOf(Props(new EparcylReminder(telegram)), "eparcyl-reminder")
}
