package kmu.reminders

import akka.actor.{Actor, Props, Status}
import com.typesafe.config.Config
import kmu.app.ApplicationComponent
import kmu.conf.KmuConf
import kmu.log.Logging
import kmu.telegram.{TelegramService, TelegramServiceComponent}

import java.time.{Duration, LocalDate, LocalDateTime, ZoneOffset}
import scala.concurrent.Await
import scala.concurrent.duration.{DurationInt, DurationLong}
import scala.language.postfixOps

class TrashReminder(
  val config: Config,
  val telegram: TelegramService
)
  extends Actor with Logging {

  import context.{dispatcher, system}

  private case object Check

  private val initialTrashDate: LocalDate = LocalDate.parse(config.getString("initial-trash-date"))

  override def preStart(): Unit = {
    system.scheduler.scheduleOnce(3 seconds, self, Check)
  }

  override def receive: Receive = {
    case Check =>
      log.info("Checking trash schedule")
      checkTrashSchedule()
      val now = LocalDateTime.now()
      val nextSchedule = now.plusDays(1).withHour(17).withMinute(30).withSecond(0)
      val scrapInterval = nextSchedule.toEpochSecond(ZoneOffset.UTC) - now.toEpochSecond(ZoneOffset.UTC)
      log.info(s"Next trash check is schedueled in ${scrapInterval} seconds")
      system.scheduler.scheduleOnce(scrapInterval seconds, self, Check)

    case Status.Failure(t) =>
      log.error(t.getMessage, t)
  }

  private def checkTrashSchedule(): Unit = {
    val today = LocalDate.now()
    // Send a reminder every 2 weeks, starting from the initial trash date
    if(Duration.between(initialTrashDate.atStartOfDay(), today.atStartOfDay()).toDays % 14 == 0) {
       sendTrashReminder()
    }
  }

  private def sendTrashReminder() = {
    val message =
      s"""
         |🗑️ C'est l'heure de sortir les poubelles !
         |Sauf si vous pensez pouvoir tenir encore 2 semaines...
      """.stripMargin
    Await.result(telegram.send(message), 10 seconds)
  }
}

trait TrashReminderComponent {
  this: ApplicationComponent
    with KmuConf
    with TelegramServiceComponent
    with Logging =>

  log.info("Starting app : Trash reminder")
  system.actorOf(Props(new TrashReminder(config, telegram)), "trash-reminder")
}