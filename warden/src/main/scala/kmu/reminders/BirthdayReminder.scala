package kmu.reminders

import akka.actor.{Actor, Props, Status}
import com.typesafe.config.Config
import kmu.app.ApplicationComponent
import kmu.conf.KmuConf
import kmu.log.Logging
import kmu.telegram.{TelegramService, TelegramServiceComponent}

import java.time.{LocalDate, LocalDateTime, ZoneOffset}
import scala.concurrent.Await
import scala.concurrent.duration.{DurationInt, DurationLong}
import scala.language.postfixOps

class BirthdayReminder(
  val birthdayRepository: BirthdayRepository,
  val config: Config,
  val telegram: TelegramService
)
  extends Actor with Logging {

  import context.{dispatcher, system}

  private case object Check

  override def preStart(): Unit = {
    initDb()
    system.scheduler.scheduleOnce(3 seconds, self, Check)
  }

  override def receive: Receive = {
    case Check =>
      log.info("Checking birthdays")
      checkBirthdays()
      val now = LocalDateTime.now()
      val nextSchedule = now.plusDays(1).withHour(0).withMinute(0).withSecond(1)
      val scrapInterval = nextSchedule.toEpochSecond(ZoneOffset.UTC) - now.toEpochSecond(ZoneOffset.UTC)
      log.info(s"Next birthday check is schedueled in ${scrapInterval} seconds")
      system.scheduler.scheduleOnce(scrapInterval seconds, self, Check)

    case Status.Failure(t) =>
      log.error(t.getMessage, t)
  }

  private def initDb(): Unit = {
    if (config.getBoolean("sqlite.dropDatabaseOnStart")) {
      log.info("Dropping birthday table")
      birthdayRepository.dropTable()
    }
    log.info("Initializing birthday table")
    birthdayRepository.createTable
  }

  private def checkBirthdays(): Unit = {
    val today = LocalDate.now()
    birthdayRepository
      .findAll()
      .filter(birthday => birthday.month == today.getMonthValue && birthday.day == today.getDayOfMonth)
      .foreach { birthday =>
        log.info(s"It's ${birthday.firstName} ${birthday.lastName}'s birthday, sending reminder")
        val age: String = birthday.year match {
          case Some(value) => (today.getYear - value).toString
          case None => "?"
        }
        sendBirthdayReminder(birthday, age)
      }
  }

  private def sendBirthdayReminder(birthday: Birthday, age: String) = {
    val message =
      s"""
         |🎂 C'est l'anniversaire de ${birthday.firstName} ${birthday.lastName} !
         |Hop hop on envoie un message pour ses *$age* ans !
      """.stripMargin
    Await.result(telegram.send(message), 10 seconds)
  }
}

trait BirthdayReminderComponent {
  this: ApplicationComponent
    with KmuConf
    with BirthdayRepositoryComponent
    with TelegramServiceComponent
    with Logging =>

  log.info("Starting app : Birthday reminder")
  system.actorOf(Props(new BirthdayReminder(birthdayRepository, config, telegram)), "birthday-reminder")
}