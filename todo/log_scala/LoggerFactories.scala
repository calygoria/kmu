package kmu.log

import ch.qos.logback.classic.{Level, Logger}
import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.ConsoleAppender
import com.typesafe.config.Config
import org.slf4j.Logger.ROOT_LOGGER_NAME
import org.slf4j.LoggerFactory
import org.slf4j.LoggerFactory.getLogger

object LoggerFactories {

  def setup(config: Config): Config = {
    val rootLogger = getLogger(ROOT_LOGGER_NAME).asInstanceOf[Logger]
    val context = rootLogger.getLoggerContext
    context.reset()

    try {
      org.slf4j.bridge.SLF4JBridgeHandler.removeHandlersForRootLogger()
      org.slf4j.bridge.SLF4JBridgeHandler.install()
    } catch {
      case e: Exception =>
    }

    val appender = new ConsoleAppender[ILoggingEvent]
    appender.setContext(context)
    appender.start()

    rootLogger.addAppender(appender)
    rootLogger.setLevel(Level.INFO)

    LoggerFactory.getLogger("akka.event.slf4j.Slf4jLogger").asInstanceOf[Logger].setLevel(Level.WARN)

    // TODO convert to scala
    import scala.collection.JavaConversions._
    for (entry <- config.withOnlyPath("logger").entrySet) {
      val level = Level.toLevel(entry.getValue.unwrapped.toString)
      if (entry.getKey == "logger._") rootLogger.setLevel(level)
      else {
        val name = if (entry.getKey.endsWith("._")) entry.getKey.substring(7, 7 + entry.getKey.length - 9)
        else entry.getKey.substring(7)
        getLogger(name).asInstanceOf[Logger].setLevel(level)
      }
    }

    config
  }
}
