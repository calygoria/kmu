package kmu.sqlite

import kmu.app.ApplicationComponent
import kmu.log.Logging

import java.sql.{DriverManager, ResultSet}

class SqliteService (dbPath: String) extends Logging {

  private val db = DriverManager.getConnection(s"jdbc:sqlite:$dbPath")

  def createTableIfNotExists(tableName: String, schema: String): Unit = {
    val statement = db.createStatement
    val query = s"CREATE TABLE IF NOT EXISTS $tableName ($schema)"
    log.debug(query)
    statement.executeUpdate(query)
  }

  def insertIfNotExists(tableName: String, schema: String, values: String): Unit = {
    val statement = db.createStatement
    val query =
      s"""
         |INSERT OR IGNORE INTO ${tableName}(${schema})
         |VALUES (${values})
         |""".stripMargin
    log.debug(query)
    statement.executeUpdate(query)
  }

  def selectAll(table: String): ResultSet = {
    val statement = db.createStatement
    statement.executeQuery(
      s"""
         |SELECT *
         |FROM $table
         |""".stripMargin)

  }

  def update(table: String, field: String, value: String, conditionField: String, conditionValue: String): Unit = {
    val statement = db.createStatement
    val query =
      s"""
         |UPDATE $table set $field = $value
         |WHERE $conditionField = $conditionValue
         |""".stripMargin
    log.debug(query)
    statement.executeUpdate(query)
  }

  def fullUpdate(table: String, fieldValueMapping: String, conditionField: String, conditionValue: String): Unit = {
    val statement = db.createStatement
    val query =
      s"""
         |UPDATE $table set $fieldValueMapping
         |WHERE $conditionField = $conditionValue
         |""".stripMargin
    log.debug(query)
    statement.executeUpdate(query)
  }

  def delete(table: String, conditionField: String, conditionValue: String): Unit = {
    val statement = db.createStatement
    val query =
      s"""
         |DELETE FROM $table
         |WHERE $conditionField = $conditionValue
         |""".stripMargin
    log.debug(query)
    statement.executeUpdate(query)

  }

  def showTable(table: String): Unit = {
    val statement = db.createStatement
    val rs = statement.executeQuery(
      s"""
        |SELECT *
        |FROM $table
        |""".stripMargin)

      while (rs.next()) {
        log.info(s"${rs.getString(3)} => ${rs.getString(2)}")
      }
    }

  def showTables(): Unit = {
    val rs = db
      .getMetaData
      .getTables(null,null,null,null)
    while (rs.next()){
      log.info(s"${rs.getString(4)} => ${rs.getString(3)}")
    }
    rs.close()
  }

  def dropTable(table: String): Unit = {
    val statement = db.createStatement
    statement.executeUpdate(s"DROP TABLE $table")

  }

}

trait SqliteServiceComponent {
  this: ApplicationComponent =>

  lazy val sqlite = new SqliteService(config.getString("sqlite.path"))
}