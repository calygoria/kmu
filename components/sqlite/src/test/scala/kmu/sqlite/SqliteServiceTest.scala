package kmu.sqlite

import kmu.app.TestApplication
import org.junit.jupiter.api.Test

class SqliteServiceTest extends TestApplication with SqliteServiceComponent {

  @Test
  def showTables(): Unit = {
    sqlite.dropTable("test")
    sqlite.createTableIfNotExists("test", "id integer PRIMARY KEY AUTOINCREMENT, name_test string UNIQUE")
    sqlite.insertIfNotExists("test", "id, name_test", "1, \"test\"")
    sqlite.insertIfNotExists("test", "name_test", "\"test2\"")
    sqlite.showTables()
    sqlite.showTable("test")
  }

  @Test
  def dropTestTable(): Unit = {
    sqlite.dropTable("test")
  }


  @Test
  def dropArtifactTable(): Unit = {
    sqlite.dropTable("artifacts")
  }

}
