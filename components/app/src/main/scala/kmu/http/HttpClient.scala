package kmu.http

import akka.actor.ActorSystem
import akka.http.scaladsl.HttpExt
import akka.http.scaladsl.model.HttpEntity.Strict
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model._
import akka.util.ByteString
import kmu.log.Logging

import java.net.URLEncoder
import java.nio.charset.StandardCharsets
import scala.concurrent.Future
import scala.collection.immutable

class HttpClient()(implicit val http: HttpExt, val system: ActorSystem) extends Logging {

  import http.system.dispatcher

  private val UTF_8 = StandardCharsets.UTF_8.name()

  def uri(url: String, params: Map[String, String] = Map.empty, encode: Boolean = true): Uri = {
    val query = params.map { case (key, value) => s"$key=${if (encode) URLEncoder.encode(value, UTF_8) else value}" }.mkString("&")
    if (query.nonEmpty) {
      Uri(s"$url?$query")
    }
    else {
      Uri(url)
    }
  }

  @inline private def debug(request: HttpRequest): Unit = {
    log.info(s"${request withEntity Strict(request.entity.contentType, ByteString(""))}")
    log.debug {
      s"""
         |--> ${request.method.name()} ${request.uri}
         |${request.headers.map(header => s"${header.name()}: ${header.value()}").mkString("\n")}
    """
        .stripMargin
    }
  }

  @inline private def debug(response: HttpResponse, body: ByteString): Unit = {
    log.info(s"${response withEntity Strict(response.entity.contentType, ByteString(""))}")
    log.debug {
      s"""
         |<-- ${response.status.intValue()}: ${response.status.reason()}
         |${response.headers.map(header => s"${header.name()}: ${header.value()}").mkString("\n")}
         |---
         |${body.utf8String}
    """
        .stripMargin
    }
  }

  def execute(request: HttpRequest): Future[ByteString] = {
//    val url = request.uri.toString()
//    val log = !url.endsWith("/api/prom/push") && !url.endsWith("/loki/api/v1/push") && !url.endsWith("/_bulk")
    val log = false

    if (log) debug(request)
    http
      .singleRequest(request)
      .flatMap {
        case resp@HttpResponse(_, _, entity, _) =>
          entity
            .dataBytes
            .runFold(ByteString(""))(_ ++ _)
            .map((resp, _))
      }
      .map {
        case (resp@HttpResponse(OK | Created | Accepted | NonAuthoritativeInformation | NoContent | ResetContent | PartialContent | MultiStatus | AlreadyReported | IMUsed, _, _, _), entity) =>
          if (log) debug(resp, entity)
          entity

        case (resp@HttpResponse(code, _, _, _), entity) =>
          if (log) debug(resp, entity)
          throw HttpException(code, entity.utf8String)
      }
  }

  def getHttpResponse(url: String): Future[HttpResponse] = {
    http
      .singleRequest(HttpRequest(uri = uri(url)))
  }

  @inline def get(uri: Uri): Future[ByteString] = execute(HttpRequest(uri = uri))

  @inline def get(url: String): Future[ByteString] = execute(HttpRequest(uri = uri(url)))

  @inline def get(url: String, headers: Seq[HttpHeader]): Future[ByteString] = execute(HttpRequest(uri = uri(url), headers = immutable.Seq(headers: _*)))

  @inline def get(url: String, params: Map[String, String]): Future[ByteString] = execute(HttpRequest(uri = uri(url, params)))


  @inline def post(uri: Uri, data: String, contentType: ContentType): Future[ByteString] = post(uri, Seq.empty, data.getBytes(UTF_8), contentType)

  @inline def post(uri: Uri, data: Array[Byte], contentType: ContentType): Future[ByteString] = post(uri, Seq.empty, data, contentType)

  @inline def post(uri: Uri, headers: Seq[HttpHeader], data: String, contentType: ContentType): Future[ByteString] = post(uri, headers, data.getBytes(UTF_8), contentType)

  @inline def post(uri: Uri, headers: Seq[HttpHeader], data: Array[Byte], contentType: ContentType): Future[ByteString] = execute {
    HttpRequest(
      HttpMethods.POST,
      uri,
      headers = immutable.Seq(headers: _*),
      entity = HttpEntity(contentType, data)
    )
  }

  @inline def post(url: String, params: Map[String, String]): Future[ByteString] = execute {
    HttpRequest(
      HttpMethods.POST,
      uri(url, params),
    )
  }

  @inline def post(url: String, data: String, contentType: ContentType): Future[ByteString] = post(url, data.getBytes(UTF_8), contentType, Map.empty[String, String])

  @inline def post(url: String, data: String, contentType: ContentType, params: Map[String, String]): Future[ByteString] = post(url, data.getBytes(UTF_8), contentType, params)

  @inline def post(url: String, data: Array[Byte], contentType: ContentType): Future[ByteString] = post(url, data, contentType, Map.empty[String, String])

  @inline def post(url: String, data: Array[Byte], contentType: ContentType, params: Map[String, String]): Future[ByteString] = execute {
    HttpRequest(
      HttpMethods.POST,
      uri(url, params),
      entity = HttpEntity(contentType, data)
    )
  }

  @inline def post(url: String, entity: RequestEntity): Future[ByteString] = post(uri(url), entity)

  @inline def post(url: String, params: Map[String, String], entity: RequestEntity): Future[ByteString] = post(uri(url, params), entity)

  @inline def post(uri: Uri, entity: RequestEntity): Future[ByteString] = execute {
    HttpRequest(
      HttpMethods.POST,
      uri,
      entity = entity
    )
  }


  @inline def put(url: String): Future[ByteString] = execute {
    HttpRequest(
      HttpMethods.PUT,
      uri(url)
    )
  }

  @inline def put(url: String, params: Map[String, String]): Future[ByteString] = execute {
    HttpRequest(
      HttpMethods.PUT,
      uri(url, params)
    )
  }

  @inline def put(url: String, data: Array[Byte], contentType: ContentType): Future[ByteString] = put(url, data, contentType, Map.empty[String, String])

  @inline def put(url: String, data: Array[Byte], contentType: ContentType, params: Map[String, String]): Future[ByteString] = execute {
    HttpRequest(
      HttpMethods.PUT,
      uri(url, params),
      entity = HttpEntity(contentType, data)
    )
  }
}