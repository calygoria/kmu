package kmu

package object http {

  case class HttpException(statusCode: akka.http.scaladsl.model.StatusCode, message: String) extends Exception(s"$statusCode: $message")

}
