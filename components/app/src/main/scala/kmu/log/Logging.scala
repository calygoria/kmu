package kmu.log

import org.slf4j.LoggerFactory

trait Logging {

  @transient protected lazy val log: com.typesafe.scalalogging.Logger = com.typesafe.scalalogging.Logger(LoggerFactory.getLogger(getClass))
}
