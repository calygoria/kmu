package kmu.log;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.CoreConstants;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigValue;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static org.slf4j.Logger.ROOT_LOGGER_NAME;
import static org.slf4j.LoggerFactory.getLogger;

public final class LoggerFactories {

    private LoggerFactories() {
    }

    public static Config setup(Config config) {
        final Logger rootLogger = (Logger) getLogger(ROOT_LOGGER_NAME);
        final LoggerContext context = rootLogger.getLoggerContext();
        context.reset();

        try {
            org.slf4j.bridge.SLF4JBridgeHandler.removeHandlersForRootLogger();
            org.slf4j.bridge.SLF4JBridgeHandler.install();
        }
        catch (Exception e) {
        }

        @SuppressWarnings("unchecked")
        Map<String, String> registry = (Map<String, String>) context.getObject(CoreConstants.PATTERN_RULE_REGISTRY);
        if (registry == null) {
            registry = new java.util.HashMap<>();
            context.putObject(CoreConstants.PATTERN_RULE_REGISTRY, registry);
        }
        registry.put("filter", LoggerNameFilter.class.getCanonicalName());
        registry.put("clr", ColorConverter.class.getCanonicalName());
        registry.put("wEx", ExtendedWhitespaceThrowableProxyConverter.class.getCanonicalName());

        PatternLayoutEncoder encoder = new PatternLayoutEncoder();
        encoder.setContext(context);
        encoder.setPattern("%clr(%d){faint} %clr(%5.5level) %clr([%15.15t]){faint} %filter(%cyan(%-35.35(%logger{30}:%L))): %clr(%m%wEx){error}%nopex%n");
        encoder.start();

        ConsoleAppender<ILoggingEvent> appender = new ConsoleAppender<>();
        appender.setContext(context);
        appender.setEncoder(encoder);
        appender.start();

        rootLogger.addAppender(appender);
        rootLogger.setLevel(Level.INFO);

        ((Logger) LoggerFactory.getLogger("akka.event.slf4j.Slf4jLogger")).setLevel(Level.WARN);

        for (Map.Entry<String, ConfigValue> entry : config.withOnlyPath("logger").entrySet()) {
            Level level = Level.toLevel(entry.getValue().unwrapped().toString());
            if (entry.getKey().equals("logger._")) {
                rootLogger.setLevel(level);
            }
            else {
                String name = entry.getKey().endsWith("._") ? entry.getKey().substring(7, 7 + entry.getKey().length() - 9) : entry.getKey().substring(7);
                ((Logger) getLogger(name)).setLevel(level);
            }
        }

        return config;
    }
}
