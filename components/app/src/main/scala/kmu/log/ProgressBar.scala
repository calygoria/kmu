package kmu.log

object ProgressBar {

  def apply(progress: Double, total: Double): String = {
    val value = Math.max(Math.min(100, Math.round((100.0 / total) * progress)), 0)
    val percent = if (value < 10) s"  $value" else if (value < 100) s" $value" else s"$value"

    s"${repeat('█', value.toInt)}${repeat('░', 100 - value.toInt)} $percent%"
  }

  private def repeat(char: Char, n: Int) = List.fill(n)(char).mkString
}
