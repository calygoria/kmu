package kmu.log;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.pattern.CompositeConverter;

public class LoggerNameFilter extends CompositeConverter<ILoggingEvent> {

    @Override
    protected String transform(ILoggingEvent event, String in) {
        if (event.getLoggerName().endsWith(".ProgressStage")) {
            return "\u23F1";
        }
        return in;
    }
}
