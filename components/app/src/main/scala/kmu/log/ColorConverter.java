package kmu.log;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.pattern.CompositeConverter;

import static java.util.Optional.ofNullable;

public class ColorConverter extends CompositeConverter<ILoggingEvent> {

    @Override
    protected String transform(ILoggingEvent event, String in) {
        return getStyle(event) + in + "\u001b[0m";
    }

    private String getStyle(ILoggingEvent event) {
        switch (ofNullable(getFirstOption()).orElse("")) {
            case "faint":
                return "\u001b[2m";

            case "error":
                switch (event.getLevel().toInt()) {
                    case Level.ERROR_INT:
                        return "\u001b[31m"; // RED
                    case Level.WARN_INT:
                        return "\u001b[33m"; // YELLOW
                    default:
                        return "\u001b[0m"; // RESET
                }

            default:
                switch (event.getLevel().toInt()) {
                    case Level.ERROR_INT:
                        return "\u001b[31m"; // RED
                    case Level.WARN_INT:
                        return "\u001b[33m"; // YELLOW
                    case Level.TRACE_INT:
                        return "\u001b[34m"; // BLUE
                    default:
                        return "\u001b[32m"; // GREEN
                }
        }
    }
}
