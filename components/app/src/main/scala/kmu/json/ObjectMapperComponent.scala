package kmu.json

import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.core.util.{DefaultIndenter, DefaultPrettyPrinter}
import com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES
import com.fasterxml.jackson.databind.ObjectWriter
import com.fasterxml.jackson.databind.SerializationFeature.{FAIL_ON_EMPTY_BEANS, WRITE_DATES_AS_TIMESTAMPS}
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.scala.{ClassTagExtensions, DefaultScalaModule}

import java.util.concurrent.atomic.AtomicReference


object PrettyPrinter {

  private val prettyPrinter: AtomicReference[ObjectWriter] = new AtomicReference()

  @inline def set(prettyPrinter: ObjectWriter): ObjectWriter = {
    this.prettyPrinter.set(prettyPrinter)
    prettyPrinter
  }

  @inline def prettyPrint(value: Any): String = prettyPrinter.get().writeValueAsString(value)
}

trait ObjectMapperComponent {

  implicit val jsonMapper: JsonMapper with ClassTagExtensions = {
    JsonMapper
      .builder()
//      .addModule(new CustomModule)
//      .addModule(new ProtobufModule)
      .addModule(new JavaTimeModule)
      .addModule(new Jdk8Module)
      .addModule(new DefaultScalaModule)
      .disable(FAIL_ON_EMPTY_BEANS)
      .disable(FAIL_ON_UNKNOWN_PROPERTIES)
      .serializationInclusion(Include.NON_EMPTY)
      .configure(WRITE_DATES_AS_TIMESTAMPS, false)
      .build() :: ClassTagExtensions
  }

  val prettyPrinter: ObjectWriter = {
    val printer = new DefaultPrettyPrinter()
    printer.indentArraysWith(DefaultIndenter.SYSTEM_LINEFEED_INSTANCE)
    PrettyPrinter.set(jsonMapper.writer(printer))
  }
}
