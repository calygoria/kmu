package kmu.config

import com.typesafe.config.{Config, ConfigFactory, ConfigValueFactory}
import org.slf4j.{Logger, LoggerFactory}

import java.lang.Thread.currentThread
import java.util.Collections.{emptyIterator, emptyList, list, singleton}

class Configs {

  private val log: Logger = LoggerFactory.getLogger(classOf[Configs])

  private def config(suffix: String) = {
    val resourceBaseName = "application" + suffix + ".conf"
    if (currentThread.getContextClassLoader.getResource(resourceBaseName) != null) {

      log.info("Loading {}", resourceBaseName)
      ConfigFactory.load(resourceBaseName)
    }
    else {
      log.info("Loading application.conf")
      ConfigFactory.load("application.conf")
    }
  }
}

object Configs {

  def load: Config = {
    //    val env = env
    //    val suffix = if (env != null) "-" + env
    //    else ""
    new Configs().config("")
      .withValue("akka.library-extensions", ConfigValueFactory.fromIterable(emptyList()))
      .withValue("akka.loggers", ConfigValueFactory.fromIterable(singleton("akka.event.slf4j.Slf4jLogger")))
      .withValue("akka.logging-filter", ConfigValueFactory.fromAnyRef("akka.event.slf4j.Slf4jLoggingFilter"))
      .withValue("akka.loglevel", ConfigValueFactory.fromAnyRef("DEBUG"))
      .withValue("akka.http.client.idle-timeout", ConfigValueFactory.fromAnyRef("infinite"))
      .withValue("akka.http.client.parsing.max-chunk-size", ConfigValueFactory.fromAnyRef("10m"))
      .withValue("akka.http.client.parsing.max-content-length", ConfigValueFactory.fromAnyRef("infinite"))
      .withValue("akka.http.server.request-timeout", ConfigValueFactory.fromAnyRef("infinite"))
      .resolve
  }
}