package kmu.config

import kmu.log.LoggerFactories

trait ConfigComponent {

  implicit val conf: com.typesafe.config.Config = {
    val conf = Configs.load
    LoggerFactories.setup(conf)
  }
}
