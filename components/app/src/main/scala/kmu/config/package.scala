package kmu

import com.typesafe.config.Config
import org.apache.commons.lang3.StringUtils

import scala.language.implicitConversions
import scala.util.Try

package object config {

  class ExtendedConfig(val config: Config) {

    def hasPaths(paths: String*): Boolean = paths.forall(config.hasPath)

    def isNotBlank(paths: String*): Boolean = paths.forall(path => Try(config.getString(path)).filter(StringUtils.isNotBlank).isSuccess)

    def getString(path: String, default: String): String = Try(config.getString(path)).getOrElse(default)
  }

  implicit def extendConfig(config: Config): ExtendedConfig = new ExtendedConfig(config)
}
