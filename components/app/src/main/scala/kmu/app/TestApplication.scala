package kmu.app

import kmu.actor.akka.ActorSystemComponent
import kmu.config.ConfigComponent
import kmu.json.ObjectMapperComponent
import kmu.log.Logging

import scala.concurrent.ExecutionContext

trait TestApplication
  extends ConfigComponent
    with ActorSystemComponent
    with ApplicationComponent
    with ObjectMapperComponent
    with Logging {

  implicit val executor: ExecutionContext = system.dispatcher
}