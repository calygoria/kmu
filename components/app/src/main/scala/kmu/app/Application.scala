package kmu.app

import akka.actor.ActorSystem
import akka.http.scaladsl.{Http, HttpExt}
import akka.pattern.after
import kmu.actor.akka.ActorSystemComponent
import kmu.config.Configs
import kmu.http.HttpClient
import kmu.json.ObjectMapperComponent
import kmu.log.LoggerFactories
import org.slf4j.LoggerFactory.getLogger
import com.google.common.io.Resources
import com.google.common.io.Resources.getResource

import java.nio.charset.StandardCharsets.UTF_8
import scala.concurrent.duration.DurationInt
import scala.language.{implicitConversions, postfixOps}
import scala.util.{Failure, Success, Try}

trait Application {

  protected def init(args: Array[String])(implicit system: ActorSystem): Runnable

  def main(args: Array[String]): Unit = {
    try {
      Try(
        Option(Resources.toString(getResource("kmu-version"), UTF_8))
          .foreach(v => getLogger(getClass.getName).info("KMU version: {}", v))
      )

      val conf = Configs.load
      LoggerFactories.setup(conf)
      implicit val system: ActorSystem = ActorSystemComponent(conf)

      if (args.headOption.contains("--dry-init")) {
        sys.exit(0)
      }
      else {
        if (args != null && args.nonEmpty) {
          getLogger(getClass.getName).info(s"${args.mkString("[", ", ", "]")}")
        }
        val app = init(args)
        if (args.headOption.contains("--dry-run")) {
          sys.exit(0)
        }
        else {
          app.run()
        }
      }
    }
    catch {
      case t: Throwable =>
        getLogger(getClass.getName).error(t.getMessage, t)
        sys.exit(1)
    }
  }
}

trait ApplicationComponent extends ObjectMapperComponent {

  implicit val system: ActorSystem

  implicit val config: com.typesafe.config.Config = system.settings.config

  implicit val http: HttpExt = Http()(system)

  implicit val httpClient: HttpClient = new HttpClient()

  val systemExit: Try[_] => Unit = {
    case Success(_) =>
      after(3 seconds)(sys.exit(0))

    case Failure(t) =>
      system.log.error(t, t.getMessage)
      after(3 seconds)(sys.exit(1))
  }
}
