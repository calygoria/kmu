package kmu.actor.akka

import akka.actor.ActorSystem
import ch.qos.logback.classic.LoggerContext
import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.AppenderBase
import com.typesafe.config.Config
import kmu.config.ConfigComponent
import org.slf4j.Logger.ROOT_LOGGER_NAME
import org.slf4j.LoggerFactory.getILoggerFactory

trait ActorSystemComponent {
  this: ConfigComponent =>

  implicit val system: ActorSystem = ActorSystemComponent(conf)
}

object ActorSystemComponent {

  def apply(config: Config): ActorSystem = {
    val system: ActorSystem = ActorSystem.create("kmu", config)

    val appender = new AppenderBase[ILoggingEvent] {
      override def append(event: ILoggingEvent): Unit = {
        system.eventStream publish event
        if (event.getThrowableProxy != null) {
          system.eventStream publish event.getThrowableProxy
        }
      }
    }

    val loggerContext = getILoggerFactory.asInstanceOf[LoggerContext]
    appender.setContext(loggerContext)

    val rootLogger = loggerContext.getLogger(ROOT_LOGGER_NAME)
    rootLogger.addAppender(appender)

    appender.start()

    system
  }
}
