package kmu

import com.google.common.base.Optional
import com.google.common.truth._

import scala.collection.JavaConverters._
import scala.language.implicitConversions

package object test {

  def assertThat(actual: Option[_]): GuavaOptionalSubject = Truth.assertThat(Optional.fromNullable(actual.orNull))

  def assertThat(actual: Boolean): BooleanSubject = Truth.assertThat(actual.asInstanceOf[java.lang.Boolean])

  def assertThat(actual: Int): IntegerSubject = Truth.assertThat(actual.asInstanceOf[java.lang.Integer])

  def assertThat(actual: Long): LongSubject = Truth.assertThat(actual.asInstanceOf[java.lang.Long])

  def assertThat(actual: Float): FloatSubject = Truth.assertThat(actual.asInstanceOf[java.lang.Float])

  def assertThat(actual: Double): DoubleSubject = Truth.assertThat(actual.asInstanceOf[java.lang.Double])

  def assertThat(actual: String): StringSubject = Truth.assertThat(actual)

  def assertThat(actual: Array[_]): IterableSubject = {
    Truth.assertThat(actual).isNotNull()
    Truth.assertThat(actual.toSeq.asJava)
  }

  def assertThat(actual: Iterable[_]): IterableSubject = {
    Truth.assertThat(actual).isNotNull()
    Truth.assertThat(actual.asJava)
  }

  class ExtendedIterableSubject(val subject: IterableSubject) {

    def containsAnyIn(expected: Iterable[_]): Unit = subject.containsAnyIn(expected.asJava)

    def containsExactlyElementsIn(expected: Iterable[_]): Ordered = subject.containsExactlyElementsIn(expected.asJava)

    def containsNoneIn(expected: Iterable[_]): Unit = subject.containsNoneIn(expected.asJava)
  }

  implicit def extendIterableSubject(subject: IterableSubject): ExtendedIterableSubject = new ExtendedIterableSubject(subject)

  def assertThat(actual: Object): Subject = Truth.assertThat(actual)

}
