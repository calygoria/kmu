package kmu.telegram

import kmu.SUCCESS
import kmu.app.TestApplication
import kmu.log.Logging
import org.junit.jupiter.api.Test

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

class TelegramServiceTest extends TestApplication with TelegramServiceComponent with Logging {

  @Test
  def sendMessageTest(): Unit = {
    val message =
      s"""
         |Hey, this is a test message from *KMU*
         |I can't wait to send you updates on _all_ you favorite libraries
         |${SUCCESS}
      """
        .stripMargin

    val message2 =
      s"""
         |🎂 *Name*
         |Please send the 🕯️
         |Or the 🕯 ?
         |
         |`1.0` ➡ `1.1`
         |Accents : é
         |Ponctuation : ?
         |
         |📚 Check release note [here](https://calygoria.fr)
      """.stripMargin

    val message3 =
      s"""
         |It s *C Girard s birthday*
         |to blow
      """.stripMargin

    Await.result (telegram.send(message3), 10 seconds)
  }
}
