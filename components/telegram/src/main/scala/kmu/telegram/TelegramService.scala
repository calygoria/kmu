package kmu.telegram

import akka.actor.ActorSystem
import com.typesafe.config.Config
import kmu.{FAILED, SUCCESS}
import kmu.app.ApplicationComponent
import kmu.http.HttpClient
import kmu.log.Logging

import java.net.URLEncoder
import scala.concurrent.{ExecutionContext, Future}

/**
 * Markdown v2 :
 * https://core.telegram.org/bots/api#markdownv2-style
 */

class TelegramService(implicit val httpClient: HttpClient, val system: ActorSystem, val config: Config) extends Logging {

  private val blockingIOExecutor: ExecutionContext = system.dispatchers.lookup("akka.actor.default-blocking-io-dispatcher")

  private val botApiKey = config.getString("telegram.botApiKey")
  private val chatId = config.getString("telegram.chatId")

  def send(message: String): Future[String] = {

    // Telegram API is not the greatest regarding char escaping, so we have to do it manually
    val escaped_message =
      message
        .replace("-", "\\-")
        .replace("!", "\\!")
        .replace(".", "\\.")

    val url = s"https://api.telegram.org/bot$botApiKey/sendMessage" +
      s"?chat_id=${URLEncoder.encode(chatId, "UTF-8")}" +
      s"&parse_mode=MarkdownV2" +
      s"&text=${URLEncoder.encode(escaped_message, "UTF-8")}"

    httpClient
      .getHttpResponse(url)
      .map { response =>
        if (response.status.intValue() == 200) {
          SUCCESS
        } else {
          log.warn(s"URL : $url")
          log.warn(s"${response.status.toString()}")
          log.warn(s"${response.headers}")
          FAILED
        }
      }(blockingIOExecutor)
  }

}

trait TelegramServiceComponent {
  this: ApplicationComponent =>

  lazy val telegram = new TelegramService
}